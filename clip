#!/bin/sh

set -e

usage() {
	echo <<HELPEOF
Usage: clip [<file>]
Copy or paste value to clipboard.

If stdin or stdout are not tty then copying and pasting is performed
respectively.
If both stdin and stdout are tty then available targets are displayed.
When both copying and pasting outputs current clipboard selection first
and then replaces it with new from input.
If file is specified, reads from it and ignores stdin.

When called as xdg-open will copies the first command line argument to
clipboard.
HELPEOF
	exit 1
} >&2

err() {
	echo $@
	exit 1
} >&2

clipmimetype() {
	xclip -selection clipboard -t "$(mimetype --output-format %m "$1")" "$1"
}

clipout() {
	xclip -selection clipboard -out
}

clipin() {
	xclip -selection clipboard -in
}

display() {
	for t in $(xclip -selection clipboard -o -t TARGETS 2>&1)
	do
		[ "$t" = "TARGETS" ] && continue
		if echo "$t" | egrep '^(.*STRING|.*TEXT|text/.*)$' -q
		then
			echo "$t: $(xclip -selection clipboard -t "$t" -out)"
		else
			echo "$t"
		fi
	done
	exit
}

main() {
	if [ "$#" -gt 1 ]
	then
		usage
	fi

	if [ "$(basename "$0")" = "xdg-open" ]
	then
		echo "$1" | clipin
		notify-send "'$(clipout)' is copied to clipboard"
		exit
	fi

	if [ -e "$1" ]
	then
		if ! [ -f "$1" ]
		then
			err "$1 is not a file"
		fi
		if ! [ -t 1 ]
		then
			paste
		fi
		clipmimetype "$1"
		exit
	fi

	if ! [ -t 1 ]
	then
		clipout
	fi

	if ! [ -t 0 ]
	then
		clipin
	fi

	if [ -t 0 ] && [ -t 1 ]
	then
		display
	fi
}

main $@

