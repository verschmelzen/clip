include config.mk

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f clip $(DESTDIR)$(PREFIX)/bin
	ln -sf $(PREFIX)/bin/clip $(DESTDIR)$(PREFIX)/bin/xdg-open

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/clip \
		$(DESTDIR)$(PREFIX)/bin/xdg-open

.PHONY: install uninstall
